﻿using PhonetiqueSearch.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Services
{
    public class GroupClassifierService : IGroupClassifierService
    {
        ICollection<char[]> letterGroups;

        public GroupClassifierService()
        {
            letterGroups = new List<char[]>();

            letterGroups.Add("aeiou".ToArray());
            letterGroups.Add("cgjkqsxyz".ToArray());
            letterGroups.Add("bfpvw".ToArray());
            letterGroups.Add("dt".ToArray());
            letterGroups.Add("mn".ToArray());
        }

        public string ClassifyLetter(char letter)
        {
            int i = 1;
            foreach(var groupList in letterGroups)
            {
                if (groupList.Contains<char>(letter))
                {
                    return i.ToString();
                }

                i = i + 1;
            }
            return "0";
        }

        
    }
}
