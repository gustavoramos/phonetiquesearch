﻿using PhonetiqueSearch.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhonetiqueSearch.Domain;
using PhonetiqueSearch.Core.Helpers;

namespace PhonetiqueSearch.Services
{
    public class PhoneticService : IPhoneticService
    {
        public IOutputHelper _outputHelper { get; set; }

        public PhoneticService(IOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }

        public void MatchDictionaries(ProcessedDictionary inputDictionary, ProcessedDictionary fileDictionary)
        {
            foreach(var inputPair in inputDictionary.dictionary)
            {
                var mathingWords = new List<String>();

                var label = inputPair.Key;
                foreach (var filePair in fileDictionary.dictionary)
                {
                    if (filePair.Value == inputPair.Value)
                    {
                        mathingWords.Add(filePair.Key);
                    }
                }

                _outputHelper.PrintResult(inputPair.Key, mathingWords);
            }            
        }
    }
}
