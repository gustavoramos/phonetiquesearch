﻿using PhonetiqueSearch.Core.Helpers;
using PhonetiqueSearch.Core.Services;
using PhonetiqueSearch.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Services
{
    public class DictionaryService : IDictionaryService
    {
        public ITextProcessorHelper _textProcessor { get; set; }
        public IFileHelper _fileHelper { get; set; }

        public DictionaryService(ITextProcessorHelper textProcessor, IFileHelper fileHelper)
        {
            _textProcessor = textProcessor;
            _fileHelper = fileHelper;
        }

        public ProcessedDictionary FileToDictionary(String filePath)
        {
            string[] lines = _fileHelper.ReadFile(filePath);
            var processedDictionary = new Dictionary<string, string>();

            foreach (string line in lines)
            {
                var returnedString = _textProcessor.ApplyRules(line);

                processedDictionary.Add(line, returnedString);
            }

            ProcessedDictionary returnDictionary = new ProcessedDictionary();
            returnDictionary.dictionary = processedDictionary;

            return returnDictionary;
        }

        public ProcessedDictionary ListToDictionary(List<string> wordsList)
        {
            var processedDictionary = new Dictionary<string, string>();

            foreach (string line in wordsList)
            {
                var returnedString = _textProcessor.ApplyRules(line);

                processedDictionary.Add(line, returnedString);
            }

            ProcessedDictionary returnDictionary = new ProcessedDictionary();
            returnDictionary.dictionary = processedDictionary;

            return returnDictionary;
        }
    }
}
