﻿using PhonetiqueSearch.Core.Helpers;
using PhonetiqueSearch.Core.Services;
using PhonetiqueSearch.Domain;
using PhonetiqueSearch.Infrastructure.Helpers;
using PhonetiqueSearch.Services;
using System;

namespace PhonetiqueSearch.Application
{
   public  class Program
    {
        public static IInputHelper _inputHelper { get; set; }
        public static IDictionaryService _dictionaryService { get; set; }
        public static IPhoneticService _phoneticService { get; set; }

        static void Main(string[] args)
        {         
            try
            {
                InjectReferences(args);

                _inputHelper.validateInput();
                var inputWords = _inputHelper.ExtractWords();
                ProcessedDictionary dictionatryInput = _dictionaryService.ListToDictionary(inputWords);

                var FilePath = _inputHelper.ExtractFilePath();
                ProcessedDictionary dictionatryFile = _dictionaryService.FileToDictionary(FilePath);

                _phoneticService.MatchDictionaries(dictionatryInput, dictionatryFile);
            }
            catch(Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
           
        }

        static void InjectReferences(string[] args)
        {            
            var classifierService = new GroupClassifierService();
            var textProcessorHelper = new TextProcessorHelper(classifierService);

            IFileHelper _fileHelper = new FileHelper(textProcessorHelper);

            _dictionaryService = new DictionaryService(textProcessorHelper, _fileHelper);
            _phoneticService = new PhoneticService(new OutputHelper());
            _inputHelper = new InputHelper(args);
        }
    }
}
