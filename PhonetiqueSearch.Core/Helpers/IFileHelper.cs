﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Core.Helpers
{
    public interface IFileHelper
    {
        string[] ReadFile(String filePath);
    }
}
