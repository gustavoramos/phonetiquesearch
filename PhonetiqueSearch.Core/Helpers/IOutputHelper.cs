﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Core.Helpers
{
    public interface IOutputHelper
    {
        void PrintResult(string word, ICollection<string> mathingList);
    }
}
