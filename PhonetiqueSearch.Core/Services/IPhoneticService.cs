﻿using PhonetiqueSearch.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Core.Services
{
    public interface IPhoneticService
    {
        void MatchDictionaries(ProcessedDictionary inputDictionary, ProcessedDictionary fileDictionary);
    }
}
