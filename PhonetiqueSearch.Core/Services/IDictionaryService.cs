﻿using PhonetiqueSearch.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Core.Services
{
    public interface IDictionaryService
    {
        ProcessedDictionary FileToDictionary(String filePath);
        ProcessedDictionary ListToDictionary(List<string> wordsList);
    }
}
