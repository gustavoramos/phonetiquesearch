﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Core.Services
{
    public interface IGroupClassifierService
    {
        string ClassifyLetter(char letter);
    }
}
