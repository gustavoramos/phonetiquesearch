﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Domain
{
    public class ProcessedDictionary
    {
        public IDictionary<string, string> dictionary { get; set; }
    }
}
