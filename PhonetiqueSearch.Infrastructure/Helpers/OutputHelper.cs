﻿using PhonetiqueSearch.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Infrastructure.Helpers
{
    public class OutputHelper : IOutputHelper
    {
        public void PrintResult(string word, ICollection<string> mathingList)
        {
            Console.Out.Write(word);
            Console.Out.Write(" : ");
            foreach(string matchingWord in mathingList)
            {
                Console.Out.Write(matchingWord + " ");
            }
            
            Console.Out.WriteLine("");
        }
    }
}
