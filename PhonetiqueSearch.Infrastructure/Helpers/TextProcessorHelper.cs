﻿using PhonetiqueSearch.Core.Helpers;
using PhonetiqueSearch.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Infrastructure.Helpers
{
    public class TextProcessorHelper : ITextProcessorHelper
    {
        public IGroupClassifierService _classifierService;
        
        public TextProcessorHelper(IGroupClassifierService classifierService)
        {
            _classifierService = classifierService;
        }

        public String ApplyRules(String content)
        {
            content = Sanitize(content);
            content = OnlyLowerCase(content);
            content = RemoveUnnusedLetters(content);
            content = RemoveRepeatedLetters(content);
            content = ClassifyInGroups(content);
            return content;
        }        

        protected String Sanitize(String content)
        {

            return new String(content.Where(Char.IsLetter).ToArray()); ;
        }

        protected String OnlyLowerCase(String content)
        {
            return content.ToLower();
        }

        protected String RemoveRepeatedLetters(String content)
        {
            if (content.Length == 0) throw new Exception("All the words must be at least 1 char long");
            return RemoveDuplicates(content);
        }

        private string RemoveDuplicates(string s)
        {
            string s1 = "";
            s1 += s[0];
            int J = 0;
            for (int I = 0; I <= s.Length - 1; I++)
            {
                if (s[I] == s1[J]) continue;
                J += 1;
                s1 += s[I];
            }
            return s1;
        }

        protected String RemoveUnnusedLetters(String content)
        {
            if (content.Length == 0) throw new Exception("All the words must be at least 1 char long");

            var returnedContent = "" + content[0];

            for(int i=1; i< content.Length; i++)
            {
                if (IsAllowedLetter(content[i]))
                {
                    returnedContent += content[i];
                }
               
            }
            return returnedContent;

        }

        protected bool IsAllowedLetter(char c)
        {
            return c != 'a' && c != 'e' && c != 'i' && c != 'h' && c != 'o' && c != 'u' && c != 'w' && c != 'y';
        }

        private string ClassifyInGroups(string content)
        {
            if (content.Length == 0) throw new Exception("All the words must be at least 1 char long");

            var returnedContent = "";

            for (int i = 0; i < content.Length; i++)
            {
                returnedContent += _classifierService.ClassifyLetter(content[i]);
            }
            return returnedContent;
        }

    }
}
