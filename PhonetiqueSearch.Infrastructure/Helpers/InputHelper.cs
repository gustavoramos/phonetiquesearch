﻿using PhonetiqueSearch.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Infrastructure.Helpers
{
    public class InputHelper : IInputHelper
    {
        public string[] _args { get; set; }
        public List<string> wordInputList{ get; set; }
        public string FilePath { get; set; }
        private string phoneticOperator;

        public InputHelper(string[] args)
        {
            _args = args;
            FilePath = "";
            phoneticOperator = "-";
            wordInputList = new List<string>();
        }

        public List<string> ExtractWords()
        {
            return wordInputList;
        }

        public string ExtractFilePath()
        {
            return FilePath;
        }

        public void validateInput()
        {
            if (!_args.Contains<string>(phoneticOperator)) throw new Exception("The " + phoneticOperator +" operator must be sended");

            int i = 0;
            while (!_args[i].Equals(phoneticOperator))
            {
                wordInputList.Add(_args[i]);
                i++;
            }

            if (wordInputList.Count == 0) throw new Exception("The number of words in the input should be higher than zero");

            FilePath = _args[i+1];
        }
    }
}
