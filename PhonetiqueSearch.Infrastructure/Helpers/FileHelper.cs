﻿using PhonetiqueSearch.Core.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonetiqueSearch.Infrastructure.Helpers
{
    public class FileHelper : IFileHelper
    {
        public ITextProcessorHelper _textProcessor { get; set; }
        protected String _filePath;

        public FileHelper(ITextProcessorHelper textProcessor)
        {
            _textProcessor = textProcessor;
        }

        public string[] ReadFile(String filePath)
        {
            if (!File.Exists(filePath)) throw new Exception("You must provid a valid file");

            string[] lines = System.IO.File.ReadAllLines(filePath);
            return lines;
        }
    }
}
